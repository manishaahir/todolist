//showing pending todo on page load
showToDo();

//function for adding todo into localstorage
function addToDo(e) {
    e.preventDefault();

    //storing input values
    let title = document.getElementById('main-title').value;
    let category = document.getElementById('main-category').value;

    //get task from local storage
    let localStorageItems = localStorage.getItem('tasks');

    //
    let tasks;
    if(localStorageItems === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }

    let task = {
        title: title,
        category: category,
        subtask: []
    };

    tasks.push(task);

    //storing array in form of string into localStorage
    localStorage.setItem('tasks', JSON.stringify( tasks ));

    //showing stored todos
    let list = document.querySelector('ul');
    list.innerHTML = null;
    showToDo();
}

//function to show localStorage data into the page
function showToDo() {
    let todoList = JSON.parse(localStorage.getItem('tasks'));
    todoList.map((item,index) => {
        let title = item.title;
        let category = item.category;
        let subtask = item.subtask;

        let listItem = `<li>
                            <div class="row-wrapper">
                                <div class="left">
                                    <b><i>${title}</i></b>- <b>(${category})</b> 
                                    <button class="${title}" onclick="editToDo(event)">Edit</button>&nbsp;&nbsp;
                                    <button onclick="deleteToDo(event)">Remove</button>&nbsp;&nbsp;
                                    <button class="${title}" onclick="addSubtask(event)">Subtask</button>
                                    <div class="edit-block-hidden">
                                        Title:<input type="text" id="edit-title">
                                        Category:<input type="text" id="edit-category">
                                        <button class="${title}" onclick="resetToDo(event)">Reset</button>
                                    </div>
                                    <div class="subtask-block-hidden">
                                        subtask:<input type="text" id="subtask">
                                        <button class="${title}" onclick="setSubtask(event)">Add</button>
                                    </div>`;
                                    
                                    subtask.map((item,i) => {
                                        if(subtask.length != 0) {
                                            listItem += `<ul class="subtasks">
                                                            <li>
                                                                ${item} 
                                                                <button onclick="deleteSubtask(event)">Remove</button>
                                                                <button class="${item}" onclick="editSubtask(event)">Edit</button>
                                                                <div class="edit-subtask-hidden">
                                                                    <input id="edit-subtask" type="text"> 
                                                                    <button class="${item}" onclick="resetSubtask(event)">Done</button>
                                                                </div>
                                                            </li>
                                                        </ul> `;
                                        }
                                    })
                                  
                                    listItem += `
                                    </div>
                                    <div class="right">
                                        <button class="${title}" onclick="todoComplete(event)">Mark as Done</button>
                                    </div>
                                </div>    
                            </li>`;

        let list = document.querySelector('ul');
        list.innerHTML += listItem;
    })
    document.getElementById('myForm').reset();
}

//to delete the specified todo
function deleteToDo(e) {
    let todoList = JSON.parse(localStorage.getItem('tasks'));
    let item = e.target.parentElement.textContent;
    let removeItem = item.trim().substring(0,item.trim().indexOf("-"));
    todoList.map((item,index) => {
        if(removeItem === item.title) {
            todoList.splice(index,1);
            localStorage.setItem('tasks',JSON.stringify( todoList ));

            //updating list shown
            let list = document.querySelector('ul');
            list.innerHTML = null;
            showToDo();
        }
    })
}


//to edit todo
function editToDo(e) {
    let todoList = JSON.parse(localStorage.getItem('tasks'));
    todoList.map((item,i) => {
        if(item.title == e.target.className) {
            document.querySelectorAll('.edit-block-hidden')[i].style.display = "block";
        }
    })
}

//resetting the todolist after edit
function resetToDo(e) {
    let todoList = JSON.parse(localStorage.getItem('tasks'));
    let title = document.getElementById('main-title').value;
    let category = document.getElementById('main-category').value;
    todoList.map((item,i) => {
        if(item.title == e.target.className) {

            //get the value of edit text boxes
            newTitle = document.querySelectorAll('#edit-title')[i].value;
            newCategory = document.querySelectorAll('#edit-category')[i].value;

            //update the localstorage
            item.title = newTitle;
            item.category = newCategory;
            localStorage.setItem('tasks',JSON.stringify( todoList ));

            //updating list shown
            let list = document.querySelector('ul');
            list.innerHTML = null;
            showToDo();
        }
    })
}

// attaching subtask to todo 
function addSubtask(e) {
    let todoList = JSON.parse(localStorage.getItem('tasks'));
    todoList.map((item,i) => {
        if(item.title == e.target.className) {
            document.querySelectorAll('.subtask-block-hidden')[i].style.display = "block";
        }
    })
}

//set subtask in localstorage 
function setSubtask(e) {
    console.log(e.target);
    let todoList = JSON.parse(localStorage.getItem('tasks'));
    todoList.map((item,i) => {
        if(item.title == e.target.className) {
            let subtask = document.querySelectorAll('#subtask')[i].value;

            //set subtask into local storage
            item.subtask.push(subtask);
            localStorage.setItem('tasks',JSON.stringify( todoList ));

            //updating list shown
            let list = document.querySelector('ul');
            list.innerHTML = null;
            showToDo();
        }
    })
}

function deleteSubtask(e) {
    let todoList = JSON.parse(localStorage.getItem('tasks'));
    let item = e.target.parentElement.parentElement.parentElement.textContent;
    let parent = item.trim().substring(0,item.trim().indexOf("-"));
    todoList.map((item,i) => {
        if(item.title == parent) {
            let subtask = item.subtask;
            let a = e.target.parentElement.textContent;
            let removeSubtask = a.substring(0,a.indexOf("R")-1);
            subtask.map((item,i) => {
                if(item == removeSubtask) {
                    subtask.splice(i,1);
                    localStorage.setItem('tasks',JSON.stringify( todoList ));
                    let list = document.querySelector('ul');
                    list.innerHTML = null;
                    showToDo();
                }
            })
        }
    })
}

// access to edit subtask
function editSubtask(e) {
    let todoList = JSON.parse(localStorage.getItem('tasks'));
    let item = e.target.parentElement.parentElement.parentElement.textContent;
    let parent = item.trim().substring(0,item.trim().indexOf("-"));
    todoList.map((item,i) => {
        if(item.title == parent) {
            let subtask = item.subtask;
            subtask.map((item,i) => {
                if(item == e.target.className) {
                    console.log(e.target.className);
                    document.querySelectorAll('.edit-subtask-hidden')[i].style.display = 'block';
                }
            })
        }
    })
}


//store updated subtask into local storage
function resetSubtask(e) {
    let todoList = JSON.parse(localStorage.getItem('tasks'));
    let item = e.target.parentElement.parentElement.parentElement.parentElement.textContent;
    let parent = item.trim().substring(0,item.trim().indexOf("-"));
    todoList.map((item,i) => {
        if(item.title == parent) {
            let subtask = item.subtask;
            subtask.map((value,i) => {
                if(value == e.target.className) {
                    newSubtask = document.querySelectorAll('#edit-subtask')[i].value;
                    value = newSubtask;
                    localStorage.setItem('tasks',JSON.stringify( todoList ));
                    console.log(value);
                    console.log(todoList);

                    //updating list shown
                    let list = document.querySelector('ul');
                    list.innerHTML = null;
                    showToDo();
                }
            })
        }
    })
}

function todoComplete(e) {
    let todoList = JSON.parse(localStorage.getItem('tasks'));
    todoList.map((item,i) => {
        if(item.title == e.target.className) {
            document.querySelectorAll('.row-wrapper')[i].style.background = "lightPink";
        }
    })
}
